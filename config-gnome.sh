#!/bin/bash

echo
echo "#=========================="
echo "# Personnalistion et interface"
echo "#=========================="
echo

echo "#--------------------------"
echo "# Fond d'écran"

mkdir -p /home/$USER/.backgrounds 

if [ $USER = "admin_pingouin" ]
then 
	convert imageadmin.png -fill white  -pointsize 100 -gravity center  -annotate 0 "admin" /home/$USER/.backgrounds/fond.png; 
else 
	convert imagepingouin.png -fill white  -pointsize 100 -gravity center  -annotate 0 $HOSTNAME /home/$USER/.backgrounds/fond.png
fi

if [ $USER = "admin_pingouin" ]; 
  then sudo cp imagepingouin.png /usr/share/backgrounds;
fi

# changement du fond d'écran
gsettings set org.gnome.desktop.background picture-uri "file:///home/$USER/.backgrounds/fond.png"

# changement de l'écran screensaver
gsettings set org.gnome.desktop.screensaver picture-uri 'file:///usr/share/backgrounds/imagepingouin.png'


# caps lock fonctionne comme sous windows (chiffres aux lieux des majuscules accentuées)
gsettings set org.gnome.desktop.input-sources xkb-options "['caps:shiftlock']"

# supprimer les coins chauds
gsettings set org.gnome.shell enable-hot-corners false

#ubuntu-dock
# ajouter les lanceurs dans la barre latérale
gsettings set org.gnome.shell favorite-apps "['firefox.desktop', 'inkscape.desktop', 'nautilus.desktop']"

# ne pas demander le mot de passe après la mise en veille
gsettings set org.gnome.desktop.screensaver ubuntu-lock-on-suspend false
