#!/bin/bash

IDEAURL='https://download.raise3d.com/ideamaker/release/3.6.1/ideaMaker_3.6.1.4415-ubuntu_amd64.deb'
CAMOTICSURL='https://camotics.org/builds/release/debian-stable-64bit/camotics_1.2.0_amd64.deb'

wget $IDEAURL
sudo dpkg -i idea*

sudo apt install -y libqt5opengl5 libqt5websockets5 libv8-3.14.5 
wget $CAMOTICSURL
sudo dpkg -i camotics*

rm idea* camotics*
