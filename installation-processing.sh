#!/bin/bash

wget https://download.processing.org/processing-3.5.4-linux64.tgz
sudo tar -xvf processing-3.5.4-linux64.tgz -C /opt

rm processing-3.5.4-linux64.tgz

cd /opt/processing-3.5.4
sudo sh install.sh
